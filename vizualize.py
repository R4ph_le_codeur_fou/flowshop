import matplotlib.pyplot as plt
import csv

weights = [0.429, 0.673, 0.091]


def plot_solutions(weights, filename, solutions):
    bests = [[10918, 3398145], [10940, 3254394], [10943, 3248493], [10950, 3189237], [10953, 3170329], [10957, 3147774], [10961, 3142704], [10962, 3137568], [10963, 3135555], [10970, 3132388], [10974, 3115569], [10990, 3114777], [10892, 3438565], [10893, 3437895], [10903, 3437394], [10935, 3295960], [10938, 3283998], [11051, 3077658], [11062, 3077089], [11063, 3072516], [11064, 3070523], [11071, 3068452], [11096, 3066167], [
        11102, 3063964], [11106, 3061340], [11107, 3060441], [11134, 3059011], [11173, 3058774], [11174, 3058766], [11192, 3058629], [11194, 3057709], [11197, 3057701], [11200, 3057287], [11201, 3056664], [10885, 3543387], [10993, 3095909], [10998, 3085859], [10999, 3082671], [11002, 3082502], [11003, 3081254], [11004, 3078814], [11042, 3078381], [10929, 3318087], [10872, 3671304], [10874, 3655057], [10883, 3622637], [10884, 3612389]]
    maxes = [11201, 3671304]
    plt.figure()
    fig, ax = plt.subplots()
    # plot iridia solution
    makespans = [b[0] for b in bests]
    tardinesses = [b[1] for b in bests]
    plt.plot(makespans, tardinesses, 'k.')
    makespans = [s[0] for s in solutions]
    tardinesses = [s[1] for s in solutions]
    plt.plot(makespans, tardinesses, 'r.')
    agregate_score = 0
    for w in weights:
        scores_w = [w*(s[0]/maxes[0]) + (1-w)*(s[1]/maxes[1])
                    for s in solutions]
        agregate_score += min(scores_w)
    agregate_score = int(agregate_score*1000)/1000
    # Create empty plot with blank marker containing the extra label
    plt.plot([], [], ' ', label="Nombre de sol Pareto: " + str(len(solutions)))
    plt.plot([], [], ' ', label="Score: " + str(agregate_score))
    plt.legend(loc='center right')
    plt.xlabel(r"Makespan $C_{max}$")  # , 'Interpreter', 'LaTex')
    # , 'Interpreter', 'LaTex')
    plt.ylabel(r'Weighted tardiness $\sum_i^n{w_i T_i}$')
    ax.set_ylim([3e6, 6e6])
    ax.set_xlim([10500, 13000])
    plt.savefig(filename+'.pdf')


def calculate_finition_time(x, permutation):
    """
    Renvoie la matrice des temps de finition de chaque machine pour la permutation 'permutation'
    xij proviennent de la liste besoins
    t est la matrice comportant les temps de finitions de chaque objet (une ligne par objet, une colonne par machine)
    """
    t = [[0 for i in range(10)] for j in range(200)]
    k = 0
    for i in permutation:  # 200 rangées (200 objets)
        for j in range(10):  # 10 colonnes (10 machines)
            if (k == 0 and j == 0):
                t[k][j] = x[i][j+2]
            elif(k == 0):
                t[k][j] = t[k][j-1] + x[i][j+2]
            elif(j == 0):
                t[k][j] = t[k-1][j] + x[i][j+2]
            else:
                t[k][j] = max(t[k-1][j], t[k][j-1]) + x[i][j+2]
        k += 1
    return t


def weighted_tardiness(x, t):
    """ Calcule la tardiness d'un individu"""
    tardinesses = [0 for i in range(200)]
    for i in range(len(t)):
        if (t[i][9] > x[i][1]):  # verif que deadline est dépassée
            # poids * (temps de fin - deadline)
            tardinesses[i] = x[i][0]*(t[i][-1]-x[i][1])
    return sum(tardinesses)


def calculate_score(individu, permutation):
    """Calcule le score de chaque individu"""
    t = calculate_finition_time(individu, permutation)  # Temps de finition
    weight_tard = weighted_tardiness(individu, t)
    liste_des_max = [max(i) for i in t]
    makespan = max(liste_des_max)
    return (makespan, weight_tard)  # poids à choisir


def score_of_permutations(csv_matrix, all_permutations):
    """ Calcule les scores pour chaque permutation et renvoie la liste de tous les scores (l'index de chaque score est le meme que celui de sa permutation)"""
    scores = []
    for permutation in all_permutations:
        scores.append(calculate_score(csv_matrix, permutation))
    return scores


def read_csv(file):
    liste = []
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        line_count = 0
        for row in csv_reader:
            l = []
            for elem in row:
                l.append(int(elem))
            if l != []:
                liste.append(l)
                line_count += 1
        print("File loaded, there are ", line_count, " elements in the list.\n")
        return liste


csv_matrix = read_csv('./input/instance.csv')
ind = 200
it = 200
poid = 1
all_permutations = read_csv(
    './results/results_'+str(ind)+'_'+str(it)+'_'+str(poid)+'.csv')
solutions = score_of_permutations(csv_matrix, all_permutations)

plot_solutions(weights, "./graphs/results_" +
               str(ind)+"_"+str(it)+"_" + str(poid), solutions)

print("Done!")


#  Les solutions des 3 premiers  pour permut_gilles.csv sont [12327, 5742856][12058, 5646281][12271, 5671702]
