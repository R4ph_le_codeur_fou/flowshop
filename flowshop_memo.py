import csv
import time
import random
import math

random.seed(0)
MAXES = [11201, 3671304]


def read_csv(file):
    liste = []
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        line_count = 0
        for row in csv_reader:
            l = []
            for elem in row:
                l.append(int(elem))
            liste.append(l)
            line_count += 1
        print("File loaded, there are ", line_count, " elements in the list.\n")
        return liste


def write_results(output, all_permutations):
    print("Writing into csv file...")
    with open(output, 'w') as file:
        writer = csv.writer(file)
        writer.writerows(all_permutations)
    print("Saved in csv file")


def get_first_permutations(it):
    """ Renvoie une liste de it permutations de listes de 200 chiffres (de 0 à 199)"""
    liste = list(range(0, 200))
    ret = []
    for i in range(it):
        l = liste[:]
        random.shuffle(l)
        ret.append(l)
    return ret


def calculate_finition_time(x, permutation):
    """
    Renvoie la matrice des temps de finition de chaque machine pour la permutation 'permutation'
    xij proviennent de la liste besoins
    t est la matrice comportant les temps de finitions de chaque objet (une ligne par objet, une colonne par machine)
    """
    t = [[0 for i in range(10)] for j in range(200)]
    k = 0
    for i in permutation:  # 200 rangées (200 objets)
        for j in range(10):  # 10 colonnes (10 machines)
            if (k == 0 and j == 0):
                t[k][j] = x[i][j+2]
            elif(k == 0):
                t[k][j] = t[k][j-1] + x[i][j+2]
            elif(j == 0):
                t[k][j] = t[k-1][j] + x[i][j+2]
            else:
                t[k][j] = max(t[k-1][j], t[k][j-1]) + x[i][j+2]
        k += 1
    return t


def weighted_tardiness(x, t):
    """ Calcule la tardiness d'un individu sur base de sa priorité et de sa deadline"""
    tardinesses = [0 for i in range(200)]
    for i in range(len(t)):
        if (t[i][9] > x[i][1]):  # verif que deadline est dépassée
            # priorité * (temps de fin - deadline)
            tardinesses[i] = x[i][0]*(t[i][-1]-x[i][1])
    return sum(tardinesses)


def calculate_score(individu, permutation):
    """Calcule le score de chaque individu"""
    t = calculate_finition_time(individu, permutation)
    weight_tard = weighted_tardiness(individu, t)
    makespan = t[len(t)-1][9]
    score = WEIGHTS[0]*weight_tard/MAXES[1]+WEIGHTS[1]*makespan/MAXES[0]
    return score


def score_of_permutations(csv_matrix, all_permutations):
    """ Calcule les scores pour chaque permutation et renvoie la liste de tous les scores (l'index de chaque score est le meme que celui de sa permutation)"""
    scores = []
    for permutation in all_permutations:
        # permutation = mutation(permutation)
        scores.append(calculate_score(csv_matrix, permutation))
    return scores


def reproduction(scores, permutations):
    """3eme étape"""
    childlist = []
    sorted_score = scores[:]
    sorted_score.sort()
    index_best_score = []
    child1 = []
    child2 = []
    index_best_score = [scores.index(i) for i in sorted_score]
    rest = []
    """Version ou on coupe en deux mais pas à partir du début"""
    for i in range(len(index_best_score)-1):  # -1 car lorsqu'on est à
        if i not in rest:
            rest.append(i)
            rest.append(i+1)
            a = random.randint(0, 199)
            b = random.randint(0, 199)
            sep1 = min(a, b)
            sep2 = max(a, b)
            """Sélection des parents"""
            # if i+1 == len(sorted_score): # Si on arrive au dernive élément de
            #     parent2 = permutations[index_best_score[0]][:]
            # else:
            # modifier les facons de mélanger
            parent1 = permutations[index_best_score[i]][:]
            parent2 = permutations[index_best_score[i+1]][:]

            """Crossover"""
            child1 = parent1[sep1:sep2]
            for j in parent2:
                if j not in child1:
                    child1.append(j)

            child2 = parent1[:sep1] + parent1[sep2:]
            for j in parent2:
                if j not in child2:
                    child2.append(j)

            childlist.append(child1)
            childlist.append(child2)
            childlist.append(parent1)
            childlist.append(parent2)
    return childlist


def swap_mutation(population):
    for i in range(len(population)-1, 0, -1):
        x = random.random()
        if x <= 0.1:  # 1 chance sur 10 --> tout mélanger
            perm = population.pop(i)
            for j in range(len(perm)):
                switch = random.random()
                if switch <= 0.01:
                    elemtoswitch = random.randint(0, 199)
                    perm[j], perm[elemtoswitch] = perm[elemtoswitch], perm[j]
            population.append(perm)
    return population


def keep_best_of_2_generations(csv_matrix, all_permutation):
    """
    Fonction qui reçoit en paramètres la matrice des machines, les permutations(parents+enfants) et les poids
    Retourne les meilleures permutations entre les parents et les enfants
    """
    scores = score_of_permutations(csv_matrix, all_permutation)
    sorted_score = scores[:]
    sorted_score.sort()
    end = len(sorted_score)//2
    index_best_score = [scores.index(i) for i in sorted_score]
    # Ne prendre que les premiers choix (meilleurs scores)
    index_best_score = index_best_score[:end]
    to_return = []
    for k in index_best_score:
        to_return.append(all_permutation[k])
    return to_return


# nombre pair pour population number
def main(population_number, generation_number, incomming_file, output_file):
    csv_matrix = read_csv(incomming_file)
    all_permutations = get_first_permutations(population_number)
    score_of_population = score_of_permutations(
        csv_matrix, all_permutations)  # Score à minimiser
    for nb_generation in range(generation_number):  # Nombre de générations
        print(nb_generation, "/", generation_number)
        all_permutations = reproduction(score_of_population, all_permutations)
        all_permutations = swap_mutation(all_permutations)
        all_permutations = keep_best_of_2_generations(
            csv_matrix, all_permutations)
    write_results(output_file, all_permutations)
    return all_permutations


population_number = 200
iteration_number = 200
WEIGHTS = [1, 1]
# weight = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
incomming_path = "./input/instance.csv"
output_path = "./results/results_" + \
    str(population_number)+"_" + str(iteration_number) + \
    "_"+str(WEIGHTS[0]) + ".csv"


begin = time.time()
final_permutations = main(
    population_number, iteration_number, incomming_path, output_path)
end = time.time()
print("Program took :", end-begin, "seconds")
