import csv
import time
import random

# random.seed(0)  # que les random soient à chaque exécution les mêmes


def read_csv(file):
    liste = []
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=",")
        line_count = 0
        for row in csv_reader:
            l = []
            for elem in row:
                l.append(int(elem))
            liste.append(l)
            line_count += 1
        print("File loaded, there are ", line_count, " elements in the list.\n")
        return liste


def calculate_finition_time(x, permutation):
    """
    Renvoie la matrice des temps de finition de chaque machine pour la permutation 'permutation'
    xij proviennent de la liste besoins
    t est la matrice comportant les temps de finitions de chaque objet (une ligne par objet, une colonne par machine)
    """
    t = [[0 for i in range(10)] for j in range(200)]
    k = 0
    for i in permutation:  # 200 rangées (200 objets)
        for j in range(10):  # 10 colonnes (10 machines)
            if (k == 0 and j == 0):
                t[k][j] = x[i][j+2]
            elif(k == 0):
                t[k][j] = t[k][j-1] + x[i][j+2]
            elif(j == 0):
                t[k][j] = t[k-1][j] + x[i][j+2]
            else:
                t[k][j] = max(t[k-1][j], t[k][j-1]) + x[i][j+2]
        k += 1
    return t


def weighted_tardiness(x, t):
    """ Calcule la tardiness d'un individu"""
    tardinesses = [0 for i in range(200)]
    for i in range(len(t)):
        if (t[i][9] > x[i][1]):  # verif que deadline est dépassée
            # poids * (temps de fin - deadline)
            tardinesses[i] = x[i][0]*(t[i][-1]-x[i][1])
        else:
            tardinesses[i] = 0

    return sum(tardinesses)


def calculate_score(individu, permutation, weight):
    """Calcule le score de chaque individu"""
    t = calculate_finition_time(individu, permutation)  # Temps de finition
    weight_tard = weighted_tardiness(individu, t)
    liste_des_max = [max(i) for i in t]
    makespan = max(liste_des_max)
    # print("weighted_tardiness = ", weight_tard)
    # print("Makespan: ", makespan)
    score = 0
    for poid in weight:
        score += poid*0.1*weight_tard+(1-poid)*makespan
    return score/len(weight)
    # return (makespan, weight_tard)


def score_of_permutations(csv_matrix, all_permutations, weigth):
    """ Calcule les scores pour chaque permutation et renvoie la liste de tous les scores (l'index de chaque score est le meme que celui de sa permutation)"""
    scores = []
    for permutation in all_permutations:
        # permutation = mutation(permutation)
        scores.append(calculate_score(csv_matrix, permutation, weight))
    return scores


def get_first_permutations(it):
    """ Renvoie une liste de it permutations de listes de 200 chiffres (de 0 à 199)"""
    liste = list(range(0, 200))
    ret = []
    for i in range(it):
        l = liste[:]
        random.shuffle(l)
        ret.append(l)
    return ret


def reproduction(scores, permutations):
    """3eme étape"""
    childlist = []
    sorted_score = scores[:]
    sorted_score.sort()
    index_best_score = []
    child1 = []
    child2 = []
    index_best_score = [i for i in range(len(sorted_score))]
    random.shuffle(index_best_score)
    x = 2
    """Version ou on coupe en deux mais pas à partir du début"""
    for i in index_best_score:
        x += 1
        if x % 2 != 0:
            a = random.randint(0, 199)
            b = random.randint(0, 199)
            sep1 = min(a, b)
            sep2 = max(a, b)
            """Sélection"""
            if i == len(sorted_score)-1:
                parent2 = permutations[0][:]
            else:
                parent2 = permutations[i+1][:]
            parent1 = permutations[i][:]  # modifier les facons de mélanger
            # parent2 = permutations[-i][:]

            """Crossover"""
            child1 = parent1[sep1:sep2]
            for j in parent2:
                if j in (parent1[:sep1]+parent1[sep2:]):
                    child1.append(j)

            child2 = parent1[:sep1] + parent1[sep2:]
            for j in parent2:
                if j in parent1[sep1:sep2]:
                    child2.append(j)
            childlist.append(child1)
            childlist.append(child2)
            childlist.append(parent1)
            childlist.append(parent2)
    """"Version ou en coupe en deux à un endroit random"""
    # for i in index_best_score:
    #     x += 1
    #     if x % 2 != 0:
    #         """Sélection"""
    #         parent1 = permutations[i][:]  # modifier les facons de mélanger
    #         parent2 = permutations[-i][:]
    #         child1 = parent1[:separator]
    #         for j in parent2:
    #             if j in parent1[separator:]:
    #                 child1.append(j)

    #         child2 = parent1[separator:]
    #         for j in parent2:
    #             if j in parent1[:separator]:
    #                 child2.append(j)
    #         childlist.append(child1)
    #         childlist.append(child2)
    #         childlist.append(parent1)
    #         childlist.append(parent2)
    return childlist


def mutation(permutations):
    for elem in permutations:
        index = random.randint(0, 199)
        index2 = random.randint(0, 199)
        elem[index], elem[index2] = elem[index2], elem[index]
    return permutations


def swap_mutation(population, mutation_rate):
    for i in range(len(population)):
        if random.random() < mutation_rate:
            perm = population[i]
            pos1, pos2 = random.sample(range(len(perm)), 2)
            perm[pos1], perm[pos2] = perm[pos2], perm[pos1]
    return population


def keep_best_of_2_generations(csv_matrix, all_permutation, weight):
    """
    Fonction qui reçoit en paramètres la matrice des machines, les permutations(parents+enfants) et les poids
    Retourne les meilleures permutations entre les parents et les enfants
    """
    scores = score_of_permutations(csv_matrix, all_permutation, weight)
    sorted_score = scores[:]
    sorted_score.sort()
    end = len(sorted_score)//2
    # index_best_score = [scores.index(i) for i in sorted_score]
    index_best_score = []
    for i in sorted_score:  # parcourir les scores un par un de façon croissantes
        x = scores.index(i)  # x = indice du score
        # if (x not in index_best_score):  # si l'indice du score n'est pas dans la liste des index_best_score
        index_best_score.append(x)

    # Ne prendre que les premiers choix (meilleurs scores)
    index_best_score = index_best_score[:end]
    to_return = []
    for k in index_best_score:
        to_return.append(all_permutation[k])
    return to_return


def write_results(output, all_permutations):
    print("Writing into csv file...")
    with open(output, 'w') as file:
        writer = csv.writer(file)
        writer.writerows(all_permutations)
    print("Saved in csv file")


def main(population_number, generation_number, incomming_file, output_file, weight, mutation_rate):
    population_number = population_number  # nombre pair
    csv_matrix = read_csv(incomming_file)
    all_permutations = get_first_permutations(population_number)
    for nb_generation in range(generation_number):  # Nombre de générations
        if (nb_generation % 5 == 0):
            print("Génération : ", nb_generation)
        score_of_population = score_of_permutations(
            csv_matrix, all_permutations, weight)  # Score à minimiser
        all_permutations = reproduction(score_of_population, all_permutations)
        all_permutations = swap_mutation(all_permutations, mutation_rate)
        all_permutations = keep_best_of_2_generations(
            csv_matrix, all_permutations, weight)
    write_results(output_file, all_permutations)
    return all_permutations


population_number = 600
iteration_number = 50
mutation_probability = 0.1
weight = [0.6]
# weight = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
incomming_path = "./input/instance.csv"
output_path = "./results/results_" + \
    str(population_number)+"_" + str(iteration_number) + \
    "_"+str(weight[0]) + ".csv"


begin = time.time()
final_permutations = main(
    population_number, iteration_number, incomming_path, output_path, weight, mutation_probability)
end = time.time()
print("Program took :", end-begin, "seconds")
